package com.morozov.familylocationserver.account;

import com.morozov.familylocationserver.requestsme.Requestme;
import com.morozov.familylocationserver.requestsme.RequestmeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public class AccountRepositoryImpl implements AccountRepositoryCustom {
    private AccountRepository accountRepository;
    private MongoTemplate mongoTemplate;
    private RequestmeRepository requestmeRepository;

    @Autowired
    public void init(AccountRepository accountRepository, MongoTemplate mongoTemplate,
                     RequestmeRepository requestmeRepository) {
        this.accountRepository = accountRepository;
        this.mongoTemplate = mongoTemplate;
        this.requestmeRepository = requestmeRepository;
    }

    @Override
    public Account getById(UUID id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("id").is(id)), Account.class);
    }

    /**
     * Сохранение
     *
     * @param account
     */
    @Override
    @Transactional
    public Account onSave(Account account) {
        //final var accountFromStorage = accountRepository.getById(account.getId());
        //Long aLong = System.currentTimeMillis();
        //if (accountFromStorage != null) {
          //  if (account.getModified() > accountFromStorage.getModified()) {
                //account.setModified(aLong);
            //    return accountRepository.save(account);
            //}
        //} else {
            //account.setModified(aLong);
        requestmeRepository.updateByAccount(account);
        if (account.getPassword() == null) {
            Account accountBase = getById(account.getId());
            if (accountBase != null && accountBase.getPassword() != null)
                account.setPassword(accountBase.getPassword());
        }
        return accountRepository.save(account);
        //}
        //return accountFromStorage;
    }

    @Override
    public Account login(String email, String password) {
        return mongoTemplate.findOne(Query.query(Criteria.where("email").is(email)), Account.class);
    }

    @Override
    public void changePassword(UUID id, String password) {
        mongoTemplate.updateFirst(
                Query.query(Criteria.where("_id").is(id)),
                new Update().set("password", password),
                Account.class);
    }
}
