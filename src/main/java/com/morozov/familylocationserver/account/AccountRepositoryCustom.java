package com.morozov.familylocationserver.account;

import java.util.UUID;

public interface AccountRepositoryCustom {

    Account getById(UUID id);

    /**
     * Выполняет сохранение. Выполняется в транзакции
     *
     * @param account Сохраняемая улица
     */
    Account onSave(Account account);

    Account login(String email, String password);

    void changePassword(UUID id, String password);
}
