package com.morozov.familylocationserver.account;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account, String>, AccountRepositoryCustom {
}
