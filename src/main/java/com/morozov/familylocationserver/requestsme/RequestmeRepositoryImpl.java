package com.morozov.familylocationserver.requestsme;

import com.morozov.familylocationserver.account.Account;
import com.morozov.familylocationserver.account.AccountRepository;
import com.morozov.familylocationserver.account.AccountRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public class RequestmeRepositoryImpl implements RequestmeRepositoryCustom {
    private RequestmeRepository requestmeRepository;
    private MongoTemplate mongoTemplate;

    @Autowired
    public void init(RequestmeRepository requestmeRepository, MongoTemplate mongoTemplate) {
        this.requestmeRepository = requestmeRepository;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Requestme getById(UUID id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("id").is(id)), Requestme.class);
    }

    @Override
    @Transactional
    public Requestme onSave(Requestme requestme) {
        Requestme requestmeSave = requestmeRepository.save(requestme);
        //обновление разрешений родственников
        mongoTemplate.updateMulti(
                Query.query(Criteria.where("_id").is(requestme.getIdWho()).and("relatives.email").is(requestme.getEmailRequest())),
                new Update().set("relatives.$.isPermission", requestme.isPermission()),
                Account.class);
        mongoTemplate.updateMulti(
                Query.query(Criteria.where("_id").is(requestme.getIdWho()).and("families.relatives.email").is(requestme.getEmailRequest())),
                new Update().set("families.$[item].relatives.$[item1].isPermission", requestme.isPermission()),
                Account.class);
        Long st = System.currentTimeMillis();
        mongoTemplate.updateMulti(
                Query.query(Criteria.where("_id").is(requestme.getIdWho())),
                new Update().set("modified", st),
                Account.class);
        return requestmeSave;
    }

    @Override
    public void updateByAccount(Account account) {
        List<Requestme> list = mongoTemplate.find(Query.query(Criteria.where("idWho").is(account.getId())), Requestme.class);
        //Удаление лишних
        for (Requestme value : list) {
            if (account.getRelatives().stream().noneMatch(v -> v.getEmail().equals(value.getEmailRequest())))
                requestmeRepository.removeById(value.getId());
        }
        //Добавление новых
        account.getRelatives().forEach(v -> {
            if (list.stream().noneMatch(m -> m.getEmailRequest().equals(v.getEmail()))) {
                Requestme requestme = new Requestme();
                requestme.setId(UUID.randomUUID());
                requestme.setIdWho(account.getId());
                requestme.setEmailWho(account.getEmail());
                requestme.setEmailRequest(v.getEmail());
                requestme.setModified(account.getModified());
                requestmeRepository.save(requestme);
            }
        });
    }

    @Override
    public List<Requestme> getAllByEmail(String email) {
        return mongoTemplate.find(Query.query(
                new Criteria().orOperator(Criteria.where("emailRequest").is(email),
                        Criteria.where("emailWho").is(email))), Requestme.class);
    }

    @Override
    public List<Requestme> getAllByEmailWho(String email) {
        return mongoTemplate.find(Query.query(Criteria.where("emailWho").is(email)), Requestme.class);
    }

    @Override
    public void removeById(UUID id) {
        mongoTemplate.remove(Query.query(Criteria.where("_id").is(id)), Requestme.class);
    }

    @Override
    public List<Requestme> getByAccountId(UUID id) {
        return mongoTemplate.find(Query.query(Criteria.where("idWho").is(id)), Requestme.class);
    }

    @Override
    public Boolean getPermission(UUID whoID, String requestEmail) {
        Requestme requestme = mongoTemplate.findOne(Query.query(Criteria.where("idWho").is(whoID).and("emailRequest").is(requestEmail)), Requestme.class);
        if (requestme != null && requestme.getId() != null)
            return requestme.isPermission();
        return false;
    }
}
