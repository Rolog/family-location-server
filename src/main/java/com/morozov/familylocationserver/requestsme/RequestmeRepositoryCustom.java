package com.morozov.familylocationserver.requestsme;

import com.morozov.familylocationserver.account.Account;

import java.util.List;
import java.util.UUID;

public interface RequestmeRepositoryCustom {

    Requestme getById(UUID id);

    /**
     * Выполняет сохранение. Выполняется в транзакции
     *
     * @param requestme Сохраняемая улица
     */
    Requestme onSave(Requestme requestme);

    void updateByAccount(Account account);

    List<Requestme> getAllByEmail(String email);

    List<Requestme> getAllByEmailWho(String email);

    void removeById(UUID id);

    List<Requestme> getByAccountId(UUID id);

    Boolean getPermission(UUID whoID, String requestEmail);
}
