package com.morozov.familylocationserver.requestsme;

import com.morozov.familylocationserver.account.AccountRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RequestmeRepository extends MongoRepository<Requestme, String>, RequestmeRepositoryCustom {
}
