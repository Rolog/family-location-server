package com.morozov.familylocationserver.requestsme;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@TypeAlias("requestme")
@Document(collection = "requestsme")
public class Requestme {
    private UUID id;
    private UUID idWho;
    private String emailWho;
    private String emailRequest;
    private boolean permission;
    private Long modified;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getIdWho() {
        return idWho;
    }

    public void setIdWho(UUID idWho) {
        this.idWho = idWho;
    }

    public String getEmailWho() {
        return emailWho;
    }

    public void setEmailWho(String emailWho) {
        this.emailWho = emailWho;
    }

    public String getEmailRequest() {
        return emailRequest;
    }

    public void setEmailRequest(String emailRequest) {
        this.emailRequest = emailRequest;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
