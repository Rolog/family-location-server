package com.morozov.familylocationserver.helper;

public enum LocationType {
    //Прямоугольник
    RECTANGLE,

    //Круг
    CIRCLE,

    //Многоугольник
    POLYGON

}
