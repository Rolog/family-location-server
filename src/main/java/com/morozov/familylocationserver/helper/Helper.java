package com.morozov.familylocationserver.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Helper {
    public static <T> T JSONtoClass(String json, Class<T> tClass) {
        try {
            JsonParser parser = new JsonParser();
            String s = URLDecoder.decode(json, String.valueOf(StandardCharsets.UTF_8));
            JsonElement mJson =  parser.parse(s.substring(0, s.length()-1));
            Gson gson = new Gson();
            return tClass.cast(gson.fromJson(mJson, tClass));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> JSONtoListClass(String json, Class<T> tClas) {
        try {
            String s = URLDecoder.decode(json, String.valueOf(StandardCharsets.UTF_8));
            s = s.substring(0, s.length()-1);
            Type listType = TypeToken.getParameterized(List.class, tClas).getType();
            return new Gson().fromJson(s, listType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String classToJSON(Object object) {
        GsonBuilder builder = new GsonBuilder().disableHtmlEscaping();
        Gson gson = builder.create();
        return gson.toJson(object);
    }
}
