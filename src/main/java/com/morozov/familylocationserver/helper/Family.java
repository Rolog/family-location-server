package com.morozov.familylocationserver.helper;

import java.util.List;
import java.util.UUID;

public class Family {
    private UUID id;
    private String name;
    private List<Relative> relatives;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives) {
        this.relatives = relatives;
    }
}
