package com.morozov.familylocationserver.helper;

public class Point {
    private Integer number;
    private Integer idLocation;
    private Double lat;
    private Double lon;

    public Point() {
    }

    public Point(Integer number, Integer idLocation, Double lat, Double lon) {
        this.number = number;
        this.idLocation = idLocation;
        this.lat = lat;
        this.lon = lon;
    }

    public Point(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(Integer idLocation) {
        this.idLocation = idLocation;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
