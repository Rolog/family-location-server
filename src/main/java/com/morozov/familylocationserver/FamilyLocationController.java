package com.morozov.familylocationserver;

import com.morozov.familylocationserver.account.Account;
import com.morozov.familylocationserver.account.AccountRepository;
import com.morozov.familylocationserver.helper.Helper;
import com.morozov.familylocationserver.position.Position;
import com.morozov.familylocationserver.position.PositionRepository;
import com.morozov.familylocationserver.requestsme.Requestme;
import com.morozov.familylocationserver.requestsme.RequestmeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/rest")
public class FamilyLocationController {
    private final AccountRepository accountRepository;
    private final RequestmeRepository requestmeRepository;
    private final PositionRepository positionRepository;

    public FamilyLocationController(AccountRepository accountRepository, RequestmeRepository requestmeRepository,
                                    PositionRepository positionRepository) {
        this.accountRepository = accountRepository;
        this.requestmeRepository = requestmeRepository;
        this.positionRepository = positionRepository;
    }

    @GetMapping("/test_connect")
    public ResponseEntity<?> testConnect() {
        return ResponseEntity.ok("Ok");
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") UUID id) {
        Account account = accountRepository.getById(id);
        account.setPassword(null);
        return ResponseEntity.ok(
                accountRepository.getById(id));
    }

    @PostMapping("/account")
    public ResponseEntity<?> save(@RequestBody byte[] account) {
        Account account1 = Helper.JSONtoClass(new String(account, StandardCharsets.UTF_8), Account.class);
        Account accountSave = accountRepository.onSave(account1);
        accountSave.setPassword(null);
        return ResponseEntity.ok(accountSave);
    }

    @GetMapping("/account/getIfModified")
    public ResponseEntity<?> getIfModified(@RequestParam UUID id,
                                      @RequestParam Long timestamp) {
        Account account = accountRepository.getById(id);
        if (account != null && account.getModified() != null)
            if (account.getModified() > timestamp) {
                account.setPassword(null);
                return ResponseEntity.ok(account);
            }
        return ResponseEntity.ok(new Account());
    }

    @GetMapping("/account_login")
    public ResponseEntity<?> login(@RequestParam String email,
                                   @RequestParam String password) {
        Account account = accountRepository.login(email, password);
        if (account == null || account.getId() == null) {
            account = new Account();
            account.setId(UUID.randomUUID());
            account.setEmail(email);
            account.setPassword(password);
            account.setModified(System.currentTimeMillis());
            account.setFamilies(new ArrayList<>());
            account.setRelatives(new ArrayList<>());
            account.setLocations(new ArrayList<>());
            accountRepository.onSave(account);
        }
        if (account.getPassword().equals(password)) {
            account.setPassword(null);
            return ResponseEntity.ok(account);
        } else {
            return ResponseEntity.ok("INVALID_PASSWORD");
        }
    }

    @GetMapping("/account_check_update")
    public ResponseEntity<?> checkUpdateAccount(@RequestParam UUID id,
                                   @RequestParam Long timestamp) {
        Account account = accountRepository.getById(id);
        return ResponseEntity.ok(account == null || account.getModified() == null || !account.getModified().equals(timestamp));
    }

    @GetMapping("/account_change_password")
    public ResponseEntity<?> changePasswordAccount(@RequestParam UUID id,
                                                @RequestParam String password) {
        accountRepository.changePassword(id, password);
        return ResponseEntity.ok("Ok");
    }

    @GetMapping("/requestme/{id}")
    public ResponseEntity<?> findByIdRequestme(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(
                requestmeRepository.getById(id));
    }

    @PostMapping("/requestme")
    public ResponseEntity<?> saveRequestme(@RequestBody byte[] requestme) {
        Requestme requestme1 = Helper.JSONtoClass(new String(requestme, StandardCharsets.UTF_8), Requestme.class);
        return ResponseEntity.ok(
                requestmeRepository.onSave(requestme1));
    }

    @GetMapping("/requestme_check_update")
    public ResponseEntity<?> checkUpdateRequestme(@RequestParam UUID id,
                                                  @RequestParam Long timestamp) {
        Requestme requestme = requestmeRepository.getById(id);
        return ResponseEntity.ok(requestme == null || requestme.getModified() == null || !requestme.getModified().equals(timestamp));
    }

    @GetMapping("/requestme_get_all_by_email")
    public ResponseEntity<?> getAllRequestmeByEmailRequest(@RequestParam String email) {
        return ResponseEntity.ok(requestmeRepository.getAllByEmail(email));
    }

    @DeleteMapping("/requestme/{id}")
    public ResponseEntity<?> deleteByIdRequestme(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(
                requestmeRepository.getById(id));
    }

    @PostMapping("/positions")
    public ResponseEntity<?> savePositions(@RequestBody byte[] positions) {
        List<Position> positions1 = Helper.JSONtoListClass(new String(positions, StandardCharsets.UTF_8), Position.class);
        assert positions1 != null;
        positions1.forEach(positionRepository::onSave);
        return ResponseEntity.ok("Ok");
    }

    @GetMapping("/positions_by_family")
    public ResponseEntity<?> getPositionsByFamily(@RequestParam UUID accountId,
                                                  @RequestParam UUID familyId) {
        return ResponseEntity.ok(positionRepository.getPositionsByFamily(accountId, familyId));
    }

    @GetMapping("/path_by_relative")
    public ResponseEntity<?> getPathByRelative(@RequestParam UUID accountId,
                                               @RequestParam UUID relativeId,
                                               @RequestParam Long start,
                                               @RequestParam Long end) {
        return ResponseEntity.ok(positionRepository.getPathByRelative(accountId, relativeId, start, end));
    }
}
