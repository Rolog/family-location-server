package com.morozov.familylocationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamilyLocationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamilyLocationServerApplication.class, args);
	}

}
