package com.morozov.familylocationserver.position;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@TypeAlias("position")
@Document(collection = "positions")
public class Position {
    private UUID id;
    private UUID accountId;
    private String accountEmail;
    private Double lat;
    private Double lon;
    private Double altitude;
    private Double speed;
    private Long modified;
    private LocalDateTime modifiedLocal;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public LocalDateTime getModifiedLocal() {
        return modifiedLocal;
    }

    public void setModifiedLocal(LocalDateTime modifiedLocal) {
        this.modifiedLocal = modifiedLocal;
    }
}
