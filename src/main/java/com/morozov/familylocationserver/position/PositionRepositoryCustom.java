package com.morozov.familylocationserver.position;

import com.morozov.familylocationserver.account.Account;
import com.morozov.familylocationserver.helper.RelativeInLocation;
import com.morozov.familylocationserver.requestsme.Requestme;

import java.util.List;
import java.util.UUID;

public interface PositionRepositoryCustom {

    Position onSave(Position position);

    List<RelativeInLocation> getPositionsByFamily(UUID accountId, UUID familyId);

    List<Position> getPathByRelative(UUID accountId, UUID relativeId, Long start, Long end);

}
