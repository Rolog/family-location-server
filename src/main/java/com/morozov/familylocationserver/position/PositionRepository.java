package com.morozov.familylocationserver.position;

import com.morozov.familylocationserver.requestsme.Requestme;
import com.morozov.familylocationserver.requestsme.RequestmeRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PositionRepository extends MongoRepository<Position, String>, PositionRepositoryCustom {
}
