package com.morozov.familylocationserver.position;

import com.morozov.familylocationserver.account.Account;
import com.morozov.familylocationserver.account.AccountRepository;
import com.morozov.familylocationserver.helper.Family;
import com.morozov.familylocationserver.helper.Point;
import com.morozov.familylocationserver.helper.Relative;
import com.morozov.familylocationserver.helper.RelativeInLocation;
import com.morozov.familylocationserver.requestsme.Requestme;
import com.morozov.familylocationserver.requestsme.RequestmeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class PositionRepositoryImpl implements PositionRepositoryCustom {
    private PositionRepository positionRepository;
    private MongoTemplate mongoTemplate;
    private AccountRepository accountRepository;
    private RequestmeRepository requestmeRepository;

    @Autowired
    public void init(PositionRepository positionRepository, MongoTemplate mongoTemplate, AccountRepository accountRepository,
                     RequestmeRepository requestmeRepository) {
        this.positionRepository = positionRepository;
        this.mongoTemplate = mongoTemplate;
        this.accountRepository = accountRepository;
        this.requestmeRepository = requestmeRepository;
    }

    @Override
    @Transactional
    public Position onSave(Position position) {
        if (position.getModifiedLocal() == null)
            position.setModifiedLocal(LocalDateTime.ofInstant(Instant.ofEpochMilli(position.getModified()), ZoneId.systemDefault()));
        Position positionSave = positionRepository.save(position);
        return positionSave;
    }

    @Override
    public List<RelativeInLocation> getPositionsByFamily(UUID accountId, UUID familyId) {
        List<RelativeInLocation> list = new ArrayList<>();
        Account account = accountRepository.getById(accountId);
        if (account != null && account.getId() != null) {
            Family family = account.getFamilies().stream().filter(f -> f.getId().equals(familyId)).findFirst().orElse(new Family());
            if (family.getId() != null) {
                List<Relative> relatives = new ArrayList<>();
                List<Requestme> requestmes = requestmeRepository.getAllByEmailWho(account.getEmail());
                family.getRelatives().forEach(v -> {
                    if (requestmes.stream().anyMatch(r -> r.getEmailRequest().equals(v.getEmail()) && r.isPermission()))
                        relatives.add(v);
                });
                relatives.forEach(r -> {
                    final Boolean[] isLocation = {false};
                    Position position = getLastPositionByEmail(r.getEmail());
                    if (position != null && position.getId() != null) {
                        account.getLocations().forEach(v -> {
                            if (isInside(v.getPoints(), new Point(position.getLat(), position.getLon()))) {
                                RelativeInLocation inLocation = new RelativeInLocation();
                                inLocation.setLocationId(v.getId());
                                inLocation.setLocationName(v.getName());
                                inLocation.setRelativeId(r.getId());
                                inLocation.setRelativeName(r.getName());
                                inLocation.setLat(position.getLat());
                                inLocation.setLon(position.getLon());
                                inLocation.setModified(position.getModified());
                                list.add(inLocation);
                                isLocation[0] = true;
                            }
                        });
                        if (!isLocation[0]) {
                            RelativeInLocation inLocation = new RelativeInLocation();
                            inLocation.setRelativeId(r.getId());
                            inLocation.setRelativeName(r.getName());
                            inLocation.setLat(position.getLat());
                            inLocation.setLon(position.getLon());
                            inLocation.setModified(position.getModified());
                            list.add(inLocation);
                        }
                    }
                });
            }
        }
        return list;
    }

    @Override
    public List<Position> getPathByRelative(UUID accountId, UUID relativeId, Long start, Long end) {
        List<Position> list = new ArrayList<>();
        Account account = accountRepository.getById(accountId);
        if (account != null && account.getId() != null) {
            Relative relative = account.getRelatives().stream().filter(f -> f.getId().equals(relativeId)).findFirst().orElse(new Relative());
            if (relative.getId() != null) {
                if (requestmeRepository.getPermission(account.getId(), relative.getEmail())) {
                    Query query = new Query();
                    query.addCriteria(Criteria.where("accountEmail").is(relative.getEmail())
                            .andOperator(
                                    Criteria.where("modified").gte(start),
                                    Criteria.where("modified").lte(end)
                            ));
                    query.with(Sort.by(Sort.Direction.ASC, "modified"));
                    list = mongoTemplate.find(query, Position.class);
                }
            }
        }
        return list;
    }

    public Position getLastPositionByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("accountEmail").is(email));
        query.with(Sort.by(Sort.Direction.DESC, "modified"));
        return mongoTemplate.findOne(query, Position.class);
    }

    //region Определение нахождения родственника в локации
    public static boolean onSegment(Point p, Point q, Point r)
    {
        if (q.getLat() <= Math.max(p.getLat(), r.getLat()) && q.getLat() >= Math.min(p.getLat(), r.getLat())
                && q.getLon() <= Math.max(p.getLon(), r.getLon()) && q.getLon() >= Math.min(p.getLon(), r.getLon()))
            return true;
        return false;
    }

    public static int orientation(Point p, Point q, Point r)
    {
        double val = (q.getLon() - p.getLon()) * (r.getLat() - q.getLat()) - (q.getLat() - p.getLat()) * (r.getLon() - q.getLon());

        if (val == 0)
            return 0;
        return (val > 0) ? 1 : 2;
    }

    public static boolean doIntersect(Point p1, Point q1, Point p2, Point q2)
    {

        int o1 = orientation(p1, q1, p2);
        int o2 = orientation(p1, q1, q2);
        int o3 = orientation(p2, q2, p1);
        int o4 = orientation(p2, q2, q1);

        if (o1 != o2 && o3 != o4)
            return true;

        if (o1 == 0 && onSegment(p1, p2, q1))
            return true;

        if (o2 == 0 && onSegment(p1, q2, q1))
            return true;

        if (o3 == 0 && onSegment(p2, p1, q2))
            return true;

        if (o4 == 0 && onSegment(p2, q1, q2))
            return true;

        return false;
    }

    public static boolean isInside(List<Point> polygon, Point p)
    {
        double INF = 10000;
        int n = polygon.size()-1;
        if (n < 3)
            return false;

        Point extreme = new Point(INF, p.getLon());

        int count = 0, i = 0;
        do
        {
            int next = (i + 1) % n;
            if (doIntersect(polygon.get(i), polygon.get(next), p, extreme))
            {
                if (orientation(polygon.get(i), p, polygon.get(next)) == 0)
                    return onSegment(polygon.get(i), p, polygon.get(next));

                count++;
            }
            i = next;
        } while (i != 0);

        return (count & 1) == 1 ? true : false;
    }
    //endregion
}
