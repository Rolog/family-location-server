# О проекте  Family Location Server

Это серверная часть проекта дающего возможность определения местоположения членов семьи и удобного отображения этого местоположения.

Существует клиентская часть в виде **android**-приложения [Family Location](https://bitbucket.org/Rolog/family-location).

# Структура Family Location Server

Family Location Server - это Rest сервис-прослойка для взаимодействия приложении [Family Location
](https://bitbucket.org/Rolog/family-location) с базой данных.

База данных с которой работает Family Location Server - [MongoDB](https://www.mongodb.com/).

Для всех пользователей доступен сервер расхоложенный по адресу [family.dota2tstats.com](family.dota2tstats.com), но можно установить Family Location Server и на свой личный сервер, а потом [настроить](https://bitbucket.org/Rolog/family-location/wiki/%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8) **android**-приложение Family Location на взаимодействие с ним.

# Установка

## Ubuntu 16.04 ##

1. Установить на сервер Ubuntu java 1.8 (пример инструкции [https://andreyex.ru/ubuntu/kak-ustanovit-java-na-ubuntu-16-04/](https://andreyex.ru/ubuntu/kak-ustanovit-java-na-ubuntu-16-04/)).

2. Установить на сервер Ubuntu базу данных mongoDB по инструкции [https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/).

3. Установить порт для mongoDB - 27017.

4. Скачать последнюю версию приложения [familylocationserver.jar](https://bitbucket.org/Rolog/family-location-server/downloads/familylocationserver.jar) со страницы [Загрузки](https://bitbucket.org/Rolog/family-location-server/downloads/).

5. Размещаем скачанный файл familylocationserver.jar на сервере.

6. Делаем сервис для запуска приложения.


Создаем файл:
```
# sudo nano /etc/systemd/system/familylocationserver.service
```

Внутрь файла записываем:
```
[Unit]
Description=Family Location Server
After=syslog.target network.target mongodb.service

[Service]
User=root
ExecStart=/usr/bin/java -jar <путь к файлу>/familylocationserver.jar

[Install]
WantedBy=multi-user.target
```

Не забываем заменить **<путь к файлу>**.

7. Запускаем сервис

```
# systemctl start familylocationserver.service
```

Дополнительные команды:

```
# systemctl stop familylocationserver.service - остановить сервис
# systemctl restart familylocationserver.service - перезапустить сервис
```

8. Настроить доступ к сервису из интернета.

### Nginx ###

[Руководство для начинающих](http://nginx.org/ru/docs/beginners_guide.html)

Пример файла **etc/nginx/sites-available/<имя-сервиса>**

```
server {
   server_name                <имя-сервиса>;

   gzip                       on;
   gzip_min_length            1000;
   gzip_proxied               any;
   gzip_types                 text/plain text/css application/javascript text/javascript application/json;

    location / {
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-Host $host;
        proxy_set_header        X-Forwarded-Server $host;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        proxy_set_header        Host $http_host;
        proxy_pass              <имя-сервиса>:8085;
        proxy_redirect off;
    }
}
```

Не забываем заменить **<имя-сервиса>** и перезагрузить Nginx.

9. Проверить работоспособность сервиса перейдя по ссылке **<имя-сервиса>/rest/test_connect**.

10. В [настройках](https://bitbucket.org/Rolog/family-location/wiki/%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8) **android**-приложения [Family Location](https://bitbucket.org/Rolog/family-location) указать адрес своего сервера.