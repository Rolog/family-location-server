const gulp      = require('gulp');
const argv      = require('yargs').argv;
const readline  = require('readline-sync');
const exec      = require('child_process').exec;
const fs        = require('fs');

/**
* Туннелирование локальных портов на удаленный сервер с поддержкой подключения
*/
function tunnel() {
    let auth,
        profile;
/*
    if (!argv.profile) {
        console.error('Set parameter --profile');
        return;
    }

    const profileName = argv.profile;
*/
    const profileName = 'development';
    const PATH = {
        profile: 'tasks/' + profileName + '.tunnel.json',
        auth: 'tasks/auth/' + profileName + '.auth.json'
    };

    if (!fs.existsSync(PATH.profile)) {
        console.error('Config file "' + PATH.profile + '" for tunnel profile "' + profileName + '" does not exist.');
        return;
    }

    profile = JSON.parse(fs.readFileSync(PATH.profile, 'utf8'));

    if (fs.existsSync(PATH.auth)) {
        auth = JSON.parse(fs.readFileSync(PATH.auth, 'utf8'));
    }

    if (!auth) {
        process.stdin.isTTY = process.stdout.isTTY = true; // Fix TTY for IDEA console

        auth = {};
        auth.login = readline.question('Enter your login on server ' + profile.host + ': \r\n');
        auth.key = readline.questionPath('Enter full path to your private ssh key for server ' + profile.host + ': \r\n', {isFile: true});

        if (!fs.existsSync('tasks/auth/')) {
            fs.mkdirSync('tasks/auth/');
        }

        fs.writeFileSync(PATH.auth, JSON.stringify(auth));
    }

    if (!profile.host) {
        console.error('Property "host" with server address does not exist in tunnel configuration file.');
        return;
    }

    if (!profile.ports) {
        profile.ports = [];
    }

    if (!profile.ports.length) {
        profile.ports.push(22);
    }

    let portList = [];
    profile.ports.forEach(function(port) {
        portList.push('-L ' + port + ':127.0.0.1:' + port);
    });

    function connect() {
        let process = exec([
            '.\\tasks\\bin\\plink.exe',
            '-v',
            '-ssh',
            '-N',
            '-C',
            '-noagent',
            '-batch',
            '-i "' + auth.key + '"',
            portList.join(' '),
            auth.login + '@' + profile.host,
            profile.fingerprint ? ('-hostkey ' + profile.fingerprint) : ''
        ].join(' '));

        process.stdout.on('data', function (data) {
            console.log(data);
        });

        process.stderr.on('data', function (data) {
            console.log(data);
        });

        process.on('close', function (code) {
            if (code) {
                console.log();
                console.log('Timeout before reconnect...');
                console.log();
                setTimeout(connect, 3000);
            }
        });
    }

    connect();
}

module.exports = function() {
    gulp.task('tunnel', tunnel);
};
